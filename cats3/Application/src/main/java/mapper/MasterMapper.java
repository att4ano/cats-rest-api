package mapper;

import dto.CatDto;
import dto.MasterDto;
import entity.MasterEntity;
import lombok.experimental.UtilityClass;
import models.Cat;
import models.Master;

import java.util.ArrayList;

@UtilityClass
public class MasterMapper {
    public static MasterDto modelToDto(Master masterModel) {

        if (masterModel == null) {
            return null;
        }

        MasterDto masterDto = MasterDto.builder()
                .id(masterModel.getId())
                .name(masterModel.getName())
                .birthdayDate(masterModel.getBirthdayDate().toString())
                .cats(new ArrayList<>())
                .build();

        for (var cat : masterModel.getCats()) {
            CatDto catDto = CatDto.builder()
                    .id(cat.getId())
                    .name(cat.getName())
                    .breed(cat.getBreed())
                    .birthdayDate(cat.getBirthdayDate().toString())
                    .color(cat.getColor())
                    .masterId(masterDto.id())
                    .friends(new ArrayList<>())
                    .build();

            masterDto.cats().add(catDto);
        }

        return masterDto;
    }

    public static Master entityToModel(MasterEntity masterEntity) {

        if (masterEntity == null) {
            return null;
        }

        Master masterModel = Master.builder()
                .id(masterEntity.getId())
                .name(masterEntity.getName())
                .birthdayDate(masterEntity.getBirthdayDate())
                .cats(new ArrayList<>())
                .build();

        for (var cat : masterEntity.getCats()) {
            Cat catModel = Cat.builder()
                    .id(cat.getId())
                    .name(cat.getName())
                    .breed(cat.getBreed())
                    .color(cat.getColor())
                    .birthdayDate(cat.getBirthdayDate())
                    .master(masterModel)
                    .catFriends(new ArrayList<>())
                    .build();

            masterModel.getCats().add(catModel);
        }

        return masterModel;
    }
}