package mapper;

import models.Cat;
import dto.CatDto;
import entity.CatEntity;

import java.util.ArrayList;

public final class CatMapper {
    public static CatDto modelToDto(Cat catModel) {
        CatDto catDto = CatDto.builder()
                .id(catModel.getId())
                .name(catModel.getName())
                .breed(catModel.getBreed())
                .color(catModel.getColor())
                .birthdayDate(catModel.getBirthdayDate().toString())
                .masterId(catModel.getMaster().getId())
                .friends(new ArrayList<>())
                .build();

        for (var cat : catModel.getCatFriends()) {
            CatDto friendDto = CatDto.builder()
                    .id(cat.getId())
                    .name(cat.getName())
                    .color(cat.getBreed())
                    .breed(cat.getBreed())
                    .birthdayDate(cat.getBirthdayDate().toString())
                    .masterId(cat.getMaster().getId())
                    .friends(new ArrayList<>())
                    .build();

            catDto.friends().add(friendDto);
        }

        return catDto;
    }

    public static Cat entityToModel(CatEntity catEntity) {
        Cat catModel = Cat.builder()
                .id(catEntity.getId())
                .name(catEntity.getName())
                .breed(catEntity.getBreed())
                .color(catEntity.getColor())
                .birthdayDate(catEntity.getBirthdayDate())
                .master(MasterMapper.entityToModel(catEntity.getMaster()))
                .catFriends(new ArrayList<>())
                .build();

        for (var cat : catEntity.getCatFriends()) {
            Cat friendModel = Cat.builder()
                    .id(cat.getId())
                    .name(cat.getName())
                    .breed(cat.getBreed())
                    .color(cat.getColor())
                    .birthdayDate(cat.getBirthdayDate())
                    .master(MasterMapper.entityToModel(cat.getMaster()))
                    .catFriends(new ArrayList<>())
                    .build();

            catModel.getCatFriends().add(friendModel);
        }

        return catModel;
    }
}