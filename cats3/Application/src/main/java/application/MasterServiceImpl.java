package application;

import contracts.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.Transactional;
import mapper.CatMapper;
import mapper.MasterMapper;
import models.Cat;
import models.Master;
import org.springframework.stereotype.Service;
import repositories.CatRepository;
import repositories.MasterRepository;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
@ComponentScan(basePackages = {"repositories"})
public class MasterServiceImpl implements MasterService {

    private final MasterRepository masterRepository;
    private final CatRepository catRepository;

    @Autowired
    public MasterServiceImpl(MasterRepository masterRepository, CatRepository catRepository) {
        this.masterRepository = masterRepository;
        this.catRepository = catRepository;
    }

    @Override
    public List<Master> checkAllMasters() {
        return masterRepository
                .findAll()
                .stream()
                .map(MasterMapper::entityToModel)
                .toList();
    }

    @Override
    public Master findMasterById(Long id) {
        return MasterMapper.entityToModel(masterRepository.findMasterEntityById(id));
    }

    @Override
    public Master findMasterByName(String name) {
        return MasterMapper.entityToModel(masterRepository.findMasterEntityByName(name));
    }

    @Override
    public List<Cat> checkAllCatsOfMaster(Long id) {
        return catRepository
                .findCatByMasterId(id)
                .stream()
                .map(CatMapper::entityToModel)
                .toList();
    }

    @Override
    public void addMaster(String name, String birthday) {
        masterRepository.add(name, LocalDate.parse(birthday));
    }

    @Override
    public void deleteMaster(Long id) {
        masterRepository.deleteMasterEntityById(id);
    }

    @Override
    public void addCatToMaster(Long catId, Long masterId) {
        masterRepository.addNewCatToMaster(catId, masterId);
    }

}