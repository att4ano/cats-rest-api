package application;

import commons.NotFoundException;
import contracts.CatService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.Transactional;
import mapper.CatMapper;
import models.Cat;
import org.springframework.stereotype.Service;
import repositories.CatRepository;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
@ComponentScan(basePackages = {"repositories"})
public class CatServiceImpl implements CatService {

    private final CatRepository catRepository;

    @Autowired
    public CatServiceImpl(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public List<Cat> checkAllCats() {
        return catRepository.findAll()
                .stream()
                .map(CatMapper::entityToModel)
                .toList();
    }

    @Override
    public List<Cat> checkAllFriendsOfCat(Long catId) {
        return catRepository
                .findAllFriends(catId)
                .stream()
                .map(CatMapper::entityToModel)
                .toList();
    }

    @Override
    public Cat findCat(Long id) throws NotFoundException {
        return CatMapper.entityToModel(catRepository.findCatEntityById(id));
    }

    @Override
    public void addCat(String name, String birthday, String breed, String color) {
        catRepository.addNewEntity(name, LocalDate.parse(birthday), breed, color);
    }

    @Override
    public void deleteCat(Long id) {
        catRepository.deleteCatEntitiesById(id);
    }

    @Override
    public void makeFriends(Long firstCatId, Long secondCatId) {
        catRepository.makeFriends(firstCatId, secondCatId);
    }

    @Override
    public void endFriendship(Long firstCatId, Long secondCatId) {
        catRepository.endFriendship(firstCatId, secondCatId);
    }
}