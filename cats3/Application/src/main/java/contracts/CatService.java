package contracts;

import commons.NotFoundException;
import models.Cat;

import java.util.List;

public interface CatService {

    List<Cat> checkAllCats();

    List<Cat> checkAllFriendsOfCat(Long catId);

    Cat findCat(Long id) throws NotFoundException;

    void addCat(String name, String birthday, String breed, String color);

    void deleteCat(Long id);

    void makeFriends(Long firstCatId, Long secondCatId);

    void endFriendship(Long firstCatId, Long secondCatId);
}