package contracts;

import models.Cat;
import models.Master;

import java.util.List;

public interface MasterService {

    List<Master> checkAllMasters();

    Master findMasterById(Long id);

    Master findMasterByName(String name);

    List<Cat> checkAllCatsOfMaster(Long masterId);

    void addMaster(String name, String birthday);

    void deleteMaster(Long id);

    void addCatToMaster(Long catId, Long masterId);
}