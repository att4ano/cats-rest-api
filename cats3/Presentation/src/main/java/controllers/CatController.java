package controllers;

import commons.DomainException;
import contracts.CatService;
import dto.CatDto;
import endpointDto.FriendshipDto;
import lombok.RequiredArgsConstructor;
import mapper.CatMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cat")
@RequiredArgsConstructor
@ComponentScan(basePackages = {"application"})
public class CatController {
    private final CatService catService;

    @GetMapping("/")
    public ResponseEntity<List<CatDto>> getAllCats() {
        return ResponseEntity.ok(catService.checkAllCats()
                .stream()
                .map(CatMapper::modelToDto)
                .toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CatDto> getCatById(@PathVariable("id") Long id) throws DomainException {
        return ResponseEntity.ok(CatMapper.modelToDto(catService.findCat(id)));
    }

    @GetMapping("/friends/{id}")
    public ResponseEntity<List<CatDto>> getAllFriendsOfCat(@PathVariable("id") Long id) {
        return ResponseEntity.ok(catService.checkAllFriendsOfCat(id)
                .stream()
                .map(CatMapper::modelToDto)
                .toList());
    }

    @PostMapping("/")
    public ResponseEntity<String> addCat(@RequestBody CatDto catDto) {
        catService.addCat(catDto.name(), catDto.birthdayDate(), catDto.breed(), catDto.color());
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCat(@PathVariable("id") Long id) {
        catService.deleteCat(id);
        return ResponseEntity.ok("Success");
    }

    @PostMapping("/friends/make")
    public ResponseEntity<String> makeFriends(@RequestBody FriendshipDto friendshipDto) {
        catService.makeFriends(friendshipDto.firstId(), friendshipDto.secondId());
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/friends/end")
    public ResponseEntity<String> endFriendShip(@RequestBody FriendshipDto friendshipDto) {
        catService.endFriendship(friendshipDto.firstId(), friendshipDto.secondId());
        return ResponseEntity.ok("Success");
    }

}