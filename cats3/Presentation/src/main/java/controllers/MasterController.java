package controllers;

import contracts.MasterService;
import dto.CatDto;
import dto.MasterDto;
import lombok.RequiredArgsConstructor;
import mapper.CatMapper;
import mapper.MasterMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
@RequiredArgsConstructor
@ComponentScan(basePackages = {"controllers"})
public class MasterController {
    private final MasterService masterService;

    @GetMapping("/")
    public ResponseEntity<List<MasterDto>> getAllMasters() {
        return ResponseEntity.ok(masterService.checkAllMasters()
                .stream()
                .map(MasterMapper::modelToDto)
                .toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MasterDto> getMasterById(@PathVariable("id") long id) {
        return ResponseEntity.ok(MasterMapper.modelToDto(masterService.findMasterById(id)));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<MasterDto> getMasterByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(MasterMapper.modelToDto(masterService.findMasterByName(name)));
    }

    @GetMapping("/cats/{id}")
    public ResponseEntity<List<CatDto>> getCatsOfMaster(@PathVariable("id") long id) {
        return ResponseEntity.ok(masterService.checkAllCatsOfMaster(id)
                .stream()
                .map(CatMapper::modelToDto)
                .toList());
    }

    @PostMapping("/")
    public void addMaster(@RequestBody MasterDto masterDto) {
        masterService.addMaster(masterDto.name(), masterDto.birthdayDate());
    }

    @DeleteMapping("/{id}")
    public void deleteMaster(@PathVariable("id") Long id) {
        masterService.deleteMaster(id);
    }

    @PostMapping("/cats/{cat_id}/{master_id}")
    public void addCatToMaster(@PathVariable("cat_id") Long catId, @PathVariable("master_id") Long masterId) {
        masterService.addCatToMaster(catId, masterId);
    }

}