package repositories;

import entity.MasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface MasterRepository extends JpaRepository<MasterEntity, Long> {

    MasterEntity findMasterEntityById (Long id);

    MasterEntity findMasterEntityByName(String name);

    @Modifying
    @Query(value = "INSERT INTO public.masters (name, birthday_date) VALUES (?, ?)",
            nativeQuery = true)
    void add(@Param("name") String name, @Param("birthday_date") LocalDate birthday_date);

    void deleteMasterEntityById(Long id);

    @Modifying
    @Query(value = "UPDATE masters SET id = null WHERE id = :id",
            nativeQuery = true)
    void deleteCatFromMaster(@Param("id") Long id);

    @Modifying
    @Query(value = "UPDATE cats SET master_id = :master_id WHERE id = :cat_id", nativeQuery = true)
    void addNewCatToMaster(@Param("cat_id") Long catId, @Param("master_id") Long masterId);
}