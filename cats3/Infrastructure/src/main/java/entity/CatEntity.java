package entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "cats")
public class CatEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "birthday_date")
    private LocalDate birthdayDate;

    @Column(name = "breed")
    private String breed;

    @Column(name = "color")
    private String color;

    @Setter
    @ManyToOne
    @JoinColumn(name = "master_id")
    private MasterEntity master;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "friends",
            joinColumns = {@JoinColumn(name = "first_friend_id")},
            inverseJoinColumns = {@JoinColumn(name = "second_friend_id")}
    )
    private Set<CatEntity> catFriends;
}