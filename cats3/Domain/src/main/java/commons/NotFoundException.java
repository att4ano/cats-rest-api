package commons;

public class NotFoundException extends DomainException {
    public NotFoundException(String message) {
        super(message);
    }

    public static NotFoundException catNotFound() {
        return new NotFoundException("Cat not found");
    }

    public static NotFoundException masterNotFound() {
        return new NotFoundException("Master not found");
    }
}