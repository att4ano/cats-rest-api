package commons;

public abstract class DomainException extends Exception{

    public DomainException(String message) {
        super(message);
    }
}